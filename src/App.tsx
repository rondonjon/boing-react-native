import { registerRootComponent } from "expo"
import { useKeepAwake } from "expo-keep-awake"
import { StatusBar } from "expo-status-bar"
import { useEffect } from "react"
import { setBehaviorAsync, setVisibilityAsync } from "expo-navigation-bar"
import { BoingScreen } from "./screens/BoingScreen"

const App = () => {
  useKeepAwake()

  useEffect(() => {
    setBehaviorAsync("overlay-swipe")
    setVisibilityAsync("hidden")
  }, [])

  return (
    <>
      <StatusBar hidden />
      <BoingScreen />
    </>
  )
}

export default registerRootComponent(App)
