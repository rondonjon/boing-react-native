import { GLView } from "expo-gl"
import { Renderer } from "expo-three"
import { OrthographicCamera } from "three"
import { BoingScene } from "../threejs/scenes/BoingScene"

export const BoingScreen = () => (
  <GLView
    style={{ flex: 1 }}
    onContextCreate={async (gl) => {
      const renderer = new Renderer({ gl, antialias: true })
      renderer.setClearColor("#000010")

      const camera = new OrthographicCamera(-100, +100, 100, -100, -100, +100)

      const scene = new BoingScene()

      const render = () => {
        scene.computeFrame()

        const glSize = {
          x: gl.drawingBufferWidth,
          y: gl.drawingBufferHeight,
        }

        const ar = glSize.x / Math.max(glSize.y, 1)

        if (ar !== 0) {
          camera.top = 100 / ar
          camera.bottom = -100 / ar
          camera.updateProjectionMatrix()
        }

        renderer.setSize(glSize.x, glSize.y)
        renderer.render(scene, camera)

        gl.flush()
        gl.endFrameEXP()

        requestAnimationFrame(render)
      }

      render()
    }}
  />
)
