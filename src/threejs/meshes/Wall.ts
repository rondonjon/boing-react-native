import { Mesh, PlaneGeometry } from "three"
import { FabricUvMaterial } from "../materials/FabricUvMaterial"

export class Wall extends Mesh {
  constructor(width: number, height: number, color = 0x00ffcc, widthSegments = 20, heightSegments = 10) {
    super()
    this.geometry = new PlaneGeometry(width, height, 1, 1)
    this.material = new FabricUvMaterial(widthSegments, heightSegments, color, 0)
    this.receiveShadow = true
  }
}
