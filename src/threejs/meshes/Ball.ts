import { Mesh, SphereGeometry } from "three"
import { CheckUvMaterial } from "../materials/CheckUvMaterial"

export class Ball extends Mesh {
  constructor(radius: number, widthSegments = 16, heightSegments = 8, color1 = 0xff0000, color2 = 0xffffff) {
    super()
    this.geometry = new SphereGeometry(radius, widthSegments, heightSegments) // Model space is -1...+1
    this.material = new CheckUvMaterial(widthSegments, heightSegments, color1, color2)
  }
}
