import { Mesh, LineBasicMaterial, Vector3, BufferGeometry, Line as ThreeLine } from "three"

export class Line extends Mesh {
  constructor(from: Vector3, to: Vector3, color = 0x00ff00) {
    super()

    const material = new LineBasicMaterial({ color })
    const geometry = new BufferGeometry().setFromPoints([from, to])
    const line = new ThreeLine(geometry, material)

    this.add(line)
  }
}
