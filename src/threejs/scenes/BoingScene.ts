import { Color, Matrix4, Scene, Vector3, MathUtils, Object3D } from "three"
import { Ball } from "../meshes/Ball"
import { Wall } from "../meshes/Wall"
import { Line } from "../meshes/Line"
import { Shadow } from "../meshes/Shadow"

const wallWidth = 200
const wallHeight = 100
const ballRadius = 33
const animDurationMs = 1500
const animWidthX = 40

type State = {
  dirX: 1 | -1
  startTime: number
  startX: number
}

const now = () => new Date().getTime()

export class BoingScene extends Scene {
  public hasVisibleAxes = false
  private axes: Object3D
  private ball: Object3D
  private shadow: Object3D
  private state: State

  constructor() {
    super()

    this.state = {
      dirX: 1,
      startTime: now() - Math.random() * animDurationMs,
      startX: 0,
    }

    this.background = new Color(0x000010)

    this.ball = new Ball(ballRadius)
    this.ball.matrixAutoUpdate = false
    this.add(this.ball)

    this.shadow = new Shadow(ballRadius)
    this.shadow.matrixAutoUpdate = false
    this.add(this.shadow)

    const wall = new Wall(wallWidth, wallHeight, 0x1c9b80)
    this.add(wall)

    this.axes = new Object3D()
    this.axes.add(new Line(new Vector3(-100, 0, 0), new Vector3(100, 0, 0), 0xff0000)) // x axis, red, length 100
    this.axes.add(new Line(new Vector3(0, -100, 0), new Vector3(0, 100, 0), 0x00ff00)) // y axis, green, length 100
    this.axes.add(new Line(new Vector3(0, 0, -100), new Vector3(0, 0, 100), 0x0000ff)) // z axis, blue, length 100
    this.add(this.axes)

    this.rotateY(MathUtils.degToRad(0))
  }

  public computeFrame() {
    const { dirX, startTime, startX } = this.state
    const progressMs = now() - startTime
    const progress01 = (progressMs % animDurationMs) / animDurationMs
    let tX = startX + progress01 * animWidthX * dirX

    if (dirX === 1 && tX >= wallWidth * 0.5 - ballRadius) {
      // bounce off right
      tX = wallWidth * 0.5 - ballRadius
      this.state = {
        dirX: -1,
        startTime: now() - animDurationMs + progressMs,
        startX: startX + animWidthX,
      }
    } else if (dirX === -1 && tX <= ballRadius - wallWidth * 0.5) {
      // bounce off left
      tX = ballRadius - wallWidth * 0.5
      this.state = {
        dirX: 1,
        startTime: now() - animDurationMs + progressMs,
        startX: startX - animWidthX,
      }
    } else if (progressMs >= animDurationMs) {
      // bounce off floor
      tX = startX + animWidthX * dirX
      this.state = {
        dirX,
        startTime: now(),
        startX: startX + animWidthX * dirX,
      }
    }

    // animate ball
    const sY = Math.sin(progress01 * Math.PI) // only "upper" half of sine function (0->1->0)
    const tY = (2 * sY - 1) * (wallHeight * 0.5 - ballRadius)
    const tZ = 50
    const rZ = MathUtils.degToRad(progress01 * 360 * dirX)
    this.ball.matrix.identity()
    this.ball.matrix.multiply(new Matrix4().makeTranslation(tX, tY, tZ))
    this.ball.matrix.multiply(new Matrix4().makeRotationZ(MathUtils.degToRad(-22.5)))
    this.ball.matrix.multiply(new Matrix4().makeRotationY(rZ))
    this.shadow.matrix.identity()
    this.shadow.matrix.multiply(new Matrix4().makeTranslation(tX + 5, tY - 5, tZ - ballRadius))
    this.shadow.matrix.multiply(new Matrix4().makeRotationZ(MathUtils.degToRad(-22.5)))
    this.shadow.matrix.multiply(new Matrix4().makeRotationY(rZ))

    // hide axes if space is pressed
    this.axes.visible = this.hasVisibleAxes
  }
}
