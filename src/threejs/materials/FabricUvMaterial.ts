import { Color, ShaderMaterial } from "three"

/**
 * Material which renders a fabric structure based on the u/v texture coordinates
 * which are expected to span the entire geometry within a single 0/0 to 1/1 range.
 * The number of horizontal and vertical boxes is determined by the `widthSegments`
 * and `heightSegments` parameters.
 */
export class FabricUvMaterial extends ShaderMaterial {
  constructor(widthSegments: number, heightSegments: number, color1: number, color2: number) {
    super({
      transparent: true,
      uniforms: {
        fWidthSegments: {
          value: widthSegments,
        },
        fHeightSegments: {
          value: heightSegments,
        },
        vColorVisible: {
          value: new Color(color1),
        },
        vColorHidden: {
          value: new Color(color2),
        },
      },
      vertexShader: `
          varying vec2 vUv;
          void main() {
            vUv = uv;
            gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
          }`,
      fragmentShader: `
          uniform float fHeightSegments;
          uniform float fWidthSegments;
          uniform vec3 vColorVisible;
          uniform vec3 vColorHidden;
          varying vec2 vUv;
          void main() {
            // compute tile/subtile positions
            vec2 vTile = vec2(
              floor(vUv.x * fWidthSegments),
              floor(vUv.y * fHeightSegments)
            );
            vec2 vSubTile = vec2(
              mod(vUv.x * fWidthSegments, 1.0),
              mod(vUv.y * fHeightSegments, 1.0)
            );
            // compute state
            bool bIsFiberX = (
              vSubTile.x >= 0.45 &&
              vSubTile.x < 0.55
            );
            bool bIsFiberY = (
              vSubTile.y >= 0.45 &&
              vSubTile.y < 0.55
            );
            bool bIsMargin = (
              (vTile.x == 0.0 && vSubTile.x < 0.45) ||
              (vTile.y == 0.0 && vSubTile.y < 0.45) ||
              (vTile.x == fWidthSegments - 1.0 && vSubTile.x >= 0.55) ||
              (vTile.y == fHeightSegments - 1.0 && vSubTile.y >= 0.55)
            );
            bool bIsBorder = (
              (vTile.x == 0.0 && vSubTile.x >= 0.45) ||
              (vTile.y == 0.0 && vSubTile.y >= 0.45) ||
              (vTile.x == fWidthSegments - 1.0 && vSubTile.x < 0.55) ||
              (vTile.y == fHeightSegments - 1.0 && vSubTile.y < 0.55)
            );
            bool bOverlayMode = (
              mod(vTile.x, 2.0) < 1.0 ^^
              mod(vTile.y, 2.0) < 1.0
            );
            // compute color
            float fAlpha = (
              bIsMargin ? 0.0 :
              bIsFiberX ? 1.0 :
              bIsFiberY ? 1.0 :
              0.0
            );
            float fLight = (
              bIsMargin ? 0.0 :
              bIsBorder ? 1.0 :
              bIsFiberX && bIsFiberY ? 1.0 : 
              bIsFiberX && bOverlayMode ? 0.25 + 1.5 * abs(0.5 - vSubTile.y) :
              bIsFiberY && !bOverlayMode ? 0.25 + 1.5 * abs(0.5 - vSubTile.x) :
              1.0
            );
            gl_FragColor = vec4(vColorVisible * fLight, fAlpha);
          }`,
    })
  }
}
