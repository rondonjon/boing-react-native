const createExpoWebpackConfigAsync = require("@expo/webpack-config")

module.exports = async function (env, argv) {
  const config = await createExpoWebpackConfigAsync(env, argv)

  // Rewrite base path from absolute / to relative ./
  config.output = config.output || {}
  config.output.publicPath = "./"

  return config
}
