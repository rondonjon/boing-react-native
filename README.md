# boing-react-native

A quick port of my [boing ThreeJS web app](https://rondonjon.gitlab.io/boing) to React Native, with a few minor changes:

- `PerspectiveCamera` replaced with `OrthographicCamera`
- Frustrum always adjusted to contain 100% width
- No sounds and no header/footer menu for now

<img src="docs/screenshot.png" width="800" height="390" alt="Screenshot" />

[Live Demo](https://rondonjon.gitlab.io/boing-react-native/)

## Running the app

### Web

Just `npm run dev`

### Android (USB connection)

Prior to `npm run dev:android`, in Windows:

- install Java (e.g. OpenJRE 11)
- install Android Studio SDK Manager, install Android SDK and USB driver
- ensure that environment variables `PATH` (with java), `JAVA_HOME` and `ANDROID_SDK_HOME` are set
- Android phone:
  - unlock developer options
  - in developer options: enable USB debugging
  - connect with USB
- run `adb devices` and check if device is listed (if displayed as `unauthorized`, confirm access on phone screen)

If the build fails, hints on the root case may be revealed with:

```
cd android
./gradlew build --warning-mode=all
```

## License

Copyright © 2022 "Ron Don Jon" Daniel Kastenholz.

Released under the [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).

## History

| Version | Changes         |
| ------- | --------------- |
| 1.0.0   | Initial version |
